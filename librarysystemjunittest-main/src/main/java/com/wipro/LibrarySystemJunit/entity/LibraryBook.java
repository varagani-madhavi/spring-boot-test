package com.wipro.LibrarySystemJunit.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LibraryBook {
	@Id
	@GeneratedValue
	private Long id;
	private double cost;
	private String bookName;
	private String authorName;
}
