package com.wipro.LibrarySystemJunit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibrarySystemJunitApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibrarySystemJunitApplication.class, args);
	}

}
